"""
Public testing utility functions.
"""


from pandas._testing import (
    assert_extension_array_equal,
    assert_frame_equal,
    assert_index_equal,
    assert_series_equal,
)

__all__ = [
    "assert_extension_array_equal",
    "assert_frame_equal",
    "assert_series_equal",
    "assert_index_equal",
]

import urllib
import os

BASE_DIR = '/wwwroot';

def getFileSystemPath(inputUrl) {
  const urlPath = urllib.parse.urlparse(inputUrl).path;
  return os.path.join(BASE_DIR, urlPath);
}